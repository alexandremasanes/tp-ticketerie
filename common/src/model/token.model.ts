import {User} from "./user.model";

export interface Token {

    user: User
    expirationTime: number
    value: string
    persistent?: boolean
}