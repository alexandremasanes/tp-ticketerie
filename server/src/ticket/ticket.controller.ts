import {Body, Controller, Delete, Get, Param, Post, Put, UseGuards} from '@nestjs/common';
import {Ticket} from "../../../common/src/model/ticket.model";
import {TicketService} from "./ticket.service";
import {Response, create} from "../../../common/src/Response";
import {AuthGuard} from "../auth.guard";

@UseGuards(AuthGuard)
@Controller('tickets')
export class TicketController {

    constructor(private readonly ticketService: TicketService) {}

    @Get()
    onGetAll(): Response<Ticket[]> {
        return create(true, this.ticketService.getAll());
    }

    @Get('/:id')
    onGetOne(@Param('id') id: number): Response<Ticket> {
        const ticket: Ticket = this.ticketService.getOne(id);
        return create(
            ticket != null,
            this.ticketService.getOne(id),
            ticket == null ? "Not Found" : undefined
        );
    }

    @Post()
    onPost(@Body() ticket: Ticket): Response<Ticket> {
        return create(true, this.ticketService.add(ticket));
    }

    @Put('/:id')
    onPut(@Param('id') id: number, @Body() ticket: Ticket): Response<undefined> {
        const exists: boolean = this.ticketService.contains(id);
        if(exists)
            this.ticketService.set(id, ticket);
        return create(exists, undefined, !exists ? "Not Found" : undefined);
    }

    @Delete('/:id')
    onDelete(@Param('id') id: number): Response<undefined> {
        const exists: boolean = this.ticketService.contains(id);
        if(exists)
            this.ticketService.remove(id);
        return create(exists, undefined, !exists ? "Not Found" : undefined);
    }
}
