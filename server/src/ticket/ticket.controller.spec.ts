import { Test, TestingModule } from '@nestjs/testing';
import { TicketController } from './ticket.controller';

describe('Ticket Controller', () => {
  let module: TestingModule;
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [TicketController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: TicketController = module.get<TicketController>(TicketController);
    expect(controller).toBeDefined();
  });
});
