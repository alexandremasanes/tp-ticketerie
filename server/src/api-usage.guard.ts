import {CanActivate, ExecutionContext, Injectable, NestInterceptor} from "@nestjs/common";
import {Observable} from "rxjs/internal/Observable";

@Injectable()
export class ApiUsageGuard implements CanActivate {

    private usageCount: number = 0;

    canActivate(
        context: ExecutionContext,
    ): boolean | Promise<boolean> | Observable<boolean> {
        return ++this.usageCount < 10;
    }
}