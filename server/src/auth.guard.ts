import {BadRequestException, CanActivate, ExecutionContext, Injectable, UnauthorizedException} from '@nestjs/common';
import {Observable} from "rxjs/index";
import {JWTManager} from "./helper/jwt-manager.service";
import {UserService} from "./user/user.service";

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(
        private readonly jwtManager: JWTManager,
        private readonly userService: UserService
    ) {
    }

    canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
        let token: string;
        token = context.switchToHttp().getRequest().headers.authorization;

        if(!token)
            throw new UnauthorizedException;
        else if(!(token = token.split('Bearer ')[1]))
            throw new BadRequestException;
        else
            return this.userService.getOne(this.jwtManager.decode(token).userId) != null;

    }
}
