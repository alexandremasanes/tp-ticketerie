import { Injectable } from '@nestjs/common';
import {User} from "../../../common/src/model/user.model";
import * as mockData from "../../conf/mock-data.json";

@Injectable()
export class UserService {

    private readonly users: User[];

    constructor() {
        this.users = (mockData as any).users
    }

    getOne(id: number): User {
        return this.users.find(user => user.id == id)
    }

    getOneByEmailAddressAndHash(emailAddress: string, hash: string): User {
        return this.users.find(
            user => user.emailAddress == emailAddress &&
                user.hash == hash
        );
    }
}
