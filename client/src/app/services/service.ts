import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Injectable()
export abstract class Service {

  constructor(protected readonly httpClient: HttpClient) {}

  protected get apiServer(): string {
    return environment.apiServer;
  }
}
