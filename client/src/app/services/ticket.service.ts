import {EventEmitter, Injectable} from '@angular/core';
import {Service} from "./service";
import {Ticket} from "../../../../common/src/model/ticket.model";

import {TicketService as TicketServiceInterface} from "../../../../common/src/service/ticket.service";
import {Response} from "../../../../common/src/response";

const ENDPOINT: string = 'tickets';

@Injectable()
export class TicketService extends Service implements TicketServiceInterface {

  readonly ticketCreated: EventEmitter<Ticket> = new EventEmitter;

  add(ticket: Ticket): Promise<Response<Ticket>> {
    return this.httpClient
               .post<Response<Ticket>>(`http://${this.apiServer}/${ENDPOINT}`, ticket)
               .toPromise();
  }

  remove(id: number): Promise<Response<void>> {
    return this.httpClient
               .delete<Response<void>>(`http://${this.apiServer}/${ENDPOINT}/${id}`)
               .toPromise();
  }

  getAll(): Promise<Response<Ticket[]>> {
    return this.httpClient
               .get<Response<Ticket[]>>(`http://${this.apiServer}/${ENDPOINT}`)
               .toPromise();
  }

  getOne(id: number): Promise<Response<Ticket>> {
    return this.httpClient
               .get<Response<Ticket>>(`http://${this.apiServer}/${ENDPOINT}/${id}`)
               .toPromise();
  }

  set(id: number, newTicket: Ticket): Promise<Response<void>> {
    return this.httpClient
               .put<Response<void>>(`http://${this.apiServer}/${ENDPOINT}/${id}`, newTicket)
               .toPromise();
  }
}
