
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {UserService} from "./services/user.service";
import {Injectable} from "@angular/core";
import {Token} from "../../../common/src/model/token.model";
import {Observable} from "rxjs/internal/Observable";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private readonly userService: UserService) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    let token: Token;
    let tokenValue: string;

    if(!request.url.includes('tokens')) {
      token = this.userService.token;
      tokenValue = token ? token.value : null;

      if (tokenValue && !request.url.includes('tokens'))
        request = request.clone({
          setHeaders: {
            Authorization: tokenValue,
          },
        });
    }

    return next.handle(request);
  }
}
