import {Component, Input, OnInit} from '@angular/core';
import {UserService} from "../../../services/user.service";
import {User} from "../../../../../../common/src/model/user.model";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private readonly userService: UserService) { }

  ngOnInit() {
  }

  get user(): User {
    return this.userService.token.user;
  }

  destroyToken(): void {
    this.userService.token = null;
  }
}
