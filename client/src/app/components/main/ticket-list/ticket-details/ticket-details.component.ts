import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Ticket} from "../../../../../../../common/src/model/ticket.model";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-ticket-details',
  templateUrl: './ticket-details.component.html',
  styleUrls: ['./ticket-details.component.css']
})
export class TicketDetailsComponent implements OnInit {

  @Input()
  ticket: Ticket;

  @Output("selectTicket")
  eventEmitter: EventEmitter<number>;

  constructor(private readonly route: ActivatedRoute) {
    this.eventEmitter = new EventEmitter;
  }

  ngOnInit() {
  }

  remove(): void {
    this.eventEmitter.emit(this.ticket.id);
  }

  get currentRoute(): boolean {
    return this.route.snapshot.params['id'] == this.ticket.id;
  }
}
