import {Directive, ElementRef, HostListener, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Directive({
  selector: '[appRoutedClass]'
})
export class RoutedClassDirective implements OnInit {

  @Input()
  href: string;

  @Input()
  classToAppend: string;

  constructor(
    private readonly elementRef: ElementRef,
    private readonly route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    if('/' + this.route.snapshot.url.join('/') == this.href &&
      !this.elementRef.nativeElement.classList.contains(this.classToAppend)) {
      this.elementRef.nativeElement.classList.add(this.classToAppend);
      this.elementRef.nativeElement.scrollIntoView();
    }
  }
}
